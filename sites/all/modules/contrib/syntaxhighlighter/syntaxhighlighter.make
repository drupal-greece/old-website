; Drush Make (http://drupal.org/project/drush_make)
api = 2
core = 7.x

; Dependencies
libraries[syntaxhighlighter][download][type] = file
libraries[syntaxhighlighter][download][url] = http://alexgorbatchev.com/SyntaxHighlighter/download/download.php?sh_current
